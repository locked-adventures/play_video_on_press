#!/usr/bin/env python3
import sys
import tkinter as tk
from typing import Optional
import cv2
from PIL import Image, ImageTk
from time import time
import warnings
warnings.simplefilter("ignore")
from gpiozero import Button


class VideoPlayer:
    def __init__(
            self, master,
            video_path: Optional[str] = None,
            background_image_path: Optional[str] = None
    ):
        if video_path is None:
            video_path = '/home/pi/Videos/big_bck_bunny_720p_stereo.avi'
        self.playing = False
        self.after_id = None
        self.master = master
        self.vs = cv2.VideoCapture(video_path)
        self.fps = self.vs.get(cv2.CAP_PROP_FPS)
        print(f"Video has {self.fps} FPS")
        self.panel = tk.Label(self.master, fg='black', bg='black')
        self.panel.pack(fill="both", expand=True)
        self.background_image = None
        if background_image_path is not None:
            # with open(background_image_path) as fh:
            self.background_image = ImageTk.PhotoImage(Image.open(background_image_path))
        self.show_img(self.background_image)
        self.master.bind('<space>', self.toggle_video)
        self.button = Button(26)
        self.pin_check()

    def show_img(self, img):
        if img is None:
            self.panel.configure(image='')
            self.panel.imgtk = None
        else:
            self.panel.configure(image=img)
            self.panel.imgtk = img

    def pin_check(self):
        if self.button.is_pressed:
            self.on()
        self.master.after(25, self.pin_check)

    def off(self):
        if self.playing:
            print("Video stopped")
            self.master.after_cancel(self.after_id)
            self.show_img(self.background_image)
            self.playing = False

    def on(self):
        if not self.playing:
            print("Playing video")
            self.vs.set(cv2.CAP_PROP_POS_FRAMES, 0)
            self.show_next_frame()
            self.playing = True

    def toggle_video(self, event=None):
        if self.playing:
            self.off()
        else:
            self.on()

    def show_next_frame(self):
        t0 = time()
        ok, frame = self.vs.read()
        if ok:
            cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            imgtk = ImageTk.PhotoImage(image=Image.fromarray(cv2image))
            self.show_img(imgtk)
            t1 = time()
            td = t1 - t0
            delay = max(round(1000 * (1 / self.fps - td)), 1)
            self.after_id = self.master.after(delay, self.show_next_frame)
        else:
            self.off()


if __name__ == '__main__':
    video_path = None
    background_image_path = None
    if len(sys.argv) >= 2:
        video_path = sys.argv[1]
    if len(sys.argv) >= 3:
        background_image_path = sys.argv[2]
    root = tk.Tk()
    root.attributes('-fullscreen', True)
    root.configure(bg='black')
    root.config(cursor="none")
    app = VideoPlayer(root, video_path, background_image_path)
    root.mainloop()
    root.bind()
